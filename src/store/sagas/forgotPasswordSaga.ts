import { put, takeEvery, call, SagaReturnType } from 'redux-saga/effects';
import { startSubmit, stopSubmit, SubmissionError } from 'redux-form';
import forgotPassword from '@services/forgotPassword.service';
import { setForgotPasswordStatus } from '@store/actions/creators/modal';
import {
  ForgotPasswordRequestAction,
  FORGOT_PASSWORD_REQUEST,
} from '@store/actions/types/forgotPassword';

function* watchOnForgotPassword() {
  yield takeEvery(FORGOT_PASSWORD_REQUEST, onForgotPassword);
}

function* onForgotPassword({ payload }: ForgotPasswordRequestAction) {
  const { email } = payload;

  try {
    yield put(startSubmit('forgot-password'));
    const {
      success,
      errors,
    }: SagaReturnType<typeof forgotPassword> = yield call(forgotPassword, {
      email,
    });

    if (errors && !success) {
      throw new SubmissionError(errors);
    }

    yield put(stopSubmit('forgot-password'));
    yield put(setForgotPasswordStatus(true));
  } catch (error) {
    yield put(stopSubmit('forgot-password', error.errors));
  }
}

export default watchOnForgotPassword;
