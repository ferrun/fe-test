import { all, fork } from 'redux-saga/effects';
import signInSaga from './signInSaga';
import forgotPasswordSaga from './forgotPasswordSaga';
import postsSaga from './postsSaga';

export default function* rootSaga() {
  yield all([fork(forgotPasswordSaga), fork(signInSaga), fork(postsSaga)]);
}
