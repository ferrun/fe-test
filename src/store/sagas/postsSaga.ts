import { put, takeEvery, call, SagaReturnType, all } from 'redux-saga/effects';
import {
  getBlogPostFail,
  getBlogPostSuccess,
  getTwitterPostFail,
  getTwitterPostSuccess,
} from '@store/actions/creators/posts';
import { loadBlogPost, loadTwitterPost } from '@services/posts.service';
import {
  GET_BLOG_POST_REQUEST,
  GET_TWITTER_POST_REQUEST,
} from '@store/actions/types/posts';

function* watchOnGetPosts() {
  yield all([
    takeEvery(GET_TWITTER_POST_REQUEST, onGetTwitterPost),
    takeEvery(GET_BLOG_POST_REQUEST, onGetBlogPost),
  ]);
}

function* onGetTwitterPost() {
  try {
    const post: SagaReturnType<typeof loadTwitterPost> = yield call(
      loadTwitterPost
    );

    if (!post) {
      throw new Error();
    }

    yield put(getTwitterPostSuccess(post));
  } catch (error) {
    yield put(getTwitterPostFail());
  }
}

function* onGetBlogPost() {
  try {
    const post: SagaReturnType<typeof loadBlogPost> = yield call(loadBlogPost);

    if (!post) {
      throw new Error();
    }

    yield put(getBlogPostSuccess(post));
  } catch (error) {
    yield put(getBlogPostFail());
  }
}

export default watchOnGetPosts;
