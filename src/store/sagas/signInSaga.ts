import { put, takeEvery, call, SagaReturnType } from 'redux-saga/effects';
import { startSubmit, stopSubmit, SubmissionError } from 'redux-form';
import signIn from '@services/signIn.service';
import {
  SignInRequestAction,
  SIGN_IN_REQUEST,
} from '@store/actions/types/signIn';
import { setAuthModalState } from '@store/actions/creators/modal';

function* watchOnSignIn() {
  yield takeEvery(SIGN_IN_REQUEST, onSignIn);
}

function* onSignIn({ payload }: SignInRequestAction) {
  const { userName, password } = payload;

  try {
    yield put(startSubmit('sign-in'));
    const { success, errors }: SagaReturnType<typeof signIn> = yield call(
      signIn,
      {
        userName,
        password,
      }
    );

    if (errors || !success) {
      throw new SubmissionError(errors || { _error: 'Login failed' });
    } else {
      yield put(stopSubmit('sign-in'));
      yield put(setAuthModalState(true));
    }
  } catch (error) {
    yield put(stopSubmit('sign-in', error.errors));
  }
}

export default watchOnSignIn;
