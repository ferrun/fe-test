import { SignInRequestAction, SIGN_IN_REQUEST } from '../types/signIn';

export const signInRequest = (
  userName: string,
  password: string
): SignInRequestAction => ({
  type: SIGN_IN_REQUEST,
  payload: {
    userName,
    password,
  },
});

export default signInRequest;
