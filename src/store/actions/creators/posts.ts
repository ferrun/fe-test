import {
  GetBlogPostFailAction,
  GetBlogPostRequestAction,
  GetBlogPostSuccessAction,
  GetTwitterPostFailAction,
  GetTwitterPostRequestAction,
  GetTwitterPostSuccessAction,
  GET_BLOG_POST_FAIL,
  GET_BLOG_POST_REQUEST,
  GET_BLOG_POST_SUCCESS,
  GET_TWITTER_POST_FAIL,
  GET_TWITTER_POST_REQUEST,
  GET_TWITTER_POST_SUCCESS,
  IPost,
} from '../types/posts';

export const getTwitterPostRequest = (): GetTwitterPostRequestAction => ({
  type: GET_TWITTER_POST_REQUEST,
});

export const getTwitterPostSuccess = (
  post: IPost
): GetTwitterPostSuccessAction => ({
  type: GET_TWITTER_POST_SUCCESS,
  payload: post,
});

export const getTwitterPostFail = (): GetTwitterPostFailAction => ({
  type: GET_TWITTER_POST_FAIL,
});

export const getBlogPostRequest = (): GetBlogPostRequestAction => ({
  type: GET_BLOG_POST_REQUEST,
});

export const getBlogPostSuccess = (post: IPost): GetBlogPostSuccessAction => ({
  type: GET_BLOG_POST_SUCCESS,
  payload: post,
});

export const getBlogPostFail = (): GetBlogPostFailAction => ({
  type: GET_BLOG_POST_FAIL,
});
