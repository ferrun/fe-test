import {
  SetAuthModalStateAction,
  SetForgotPasswordStatusAction,
  SET_AUTH_MODAL_STATE,
  SET_FORGOT_PASSWORD_STATUS,
} from '../types/modals';

export const setForgotPasswordStatus = (
  hasSend: boolean
): SetForgotPasswordStatusAction => ({
  type: SET_FORGOT_PASSWORD_STATUS,
  payload: hasSend,
});

export const setAuthModalState = (
  isOpen: boolean
): SetAuthModalStateAction => ({
  type: SET_AUTH_MODAL_STATE,
  payload: isOpen,
});
