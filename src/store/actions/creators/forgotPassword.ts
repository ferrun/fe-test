import {
  ForgotPasswordRequestAction,
  FORGOT_PASSWORD_REQUEST,
} from '../types/forgotPassword';

export const forgotPasswordRequest = (
  email: string
): ForgotPasswordRequestAction => ({
  type: FORGOT_PASSWORD_REQUEST,
  payload: {
    email,
  },
});

export default forgotPasswordRequest;
