export interface IPost {
  date: Date | number;
  text: string;
}
export const GET_TWITTER_POST_REQUEST =
  'postsActionTypes/GET_TWITTER_POST_REQUEST';
export interface GetTwitterPostRequestAction {
  type: typeof GET_TWITTER_POST_REQUEST;
}

export const GET_TWITTER_POST_SUCCESS =
  'postsActionTypes/GET_TWITTER_POST_SUCCESS';
export interface GetTwitterPostSuccessAction {
  type: typeof GET_TWITTER_POST_SUCCESS;
  payload: IPost;
}

export const GET_TWITTER_POST_FAIL = 'postsActionTypes/GET_TWITTER_POST_FAIL';
export interface GetTwitterPostFailAction {
  type: typeof GET_TWITTER_POST_FAIL;
}

export const GET_BLOG_POST_REQUEST = 'modalsActionTypes/GET_BLOG_POST_REQUEST';
export interface GetBlogPostRequestAction {
  type: typeof GET_BLOG_POST_REQUEST;
}

export const GET_BLOG_POST_SUCCESS = 'modalsActionTypes/GET_BLOG_POST_SUCCESS';
export interface GetBlogPostSuccessAction {
  type: typeof GET_BLOG_POST_SUCCESS;
  payload: IPost;
}

export const GET_BLOG_POST_FAIL = 'modalsActionTypes/GET_BLOG_POST_FAIL';
export interface GetBlogPostFailAction {
  type: typeof GET_BLOG_POST_FAIL;
}

export type PostsAction =
  | GetTwitterPostRequestAction
  | GetTwitterPostSuccessAction
  | GetTwitterPostFailAction
  | GetBlogPostRequestAction
  | GetBlogPostSuccessAction
  | GetBlogPostFailAction;
