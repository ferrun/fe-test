export interface SignInRequestParams {
  userName: string;
  password: string;
}

export const SIGN_IN_REQUEST = 'signInActionTypes/SIGN_IN_REQUEST';
export interface SignInRequestAction {
  type: typeof SIGN_IN_REQUEST;
  payload: SignInRequestParams;
}

export type SignInAction = SignInRequestAction;
