export interface ForgotPasswordRequestParams {
  email: string;
}

export const FORGOT_PASSWORD_REQUEST =
  'forgotPasswordActionTypes/FORGOT_PASSWORD_REQUEST';
export interface ForgotPasswordRequestAction {
  type: typeof FORGOT_PASSWORD_REQUEST;
  payload: ForgotPasswordRequestParams;
}

export type ForgotPasswordAction = ForgotPasswordRequestAction;
