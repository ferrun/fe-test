export const SET_FORGOT_PASSWORD_STATUS =
  'modalsActionTypes/SET_FORGOT_PASSWORD_STATUS';
export interface SetForgotPasswordStatusAction {
  type: typeof SET_FORGOT_PASSWORD_STATUS;
  payload: boolean;
}

export interface SetAuthModalStateParams {
  isOpen: boolean;
}

export const SET_AUTH_MODAL_STATE = 'modalsActionTypes/SET_AUTH_MODAL_STATE';
export interface SetAuthModalStateAction {
  type: typeof SET_AUTH_MODAL_STATE;
  payload: boolean;
}

export type UserAction =
  | SetForgotPasswordStatusAction
  | SetAuthModalStateAction;
