import { combineReducers } from 'redux';
import { FormReducer, reducer as reduxFormReducer } from 'redux-form';
import { modalReducer, UserState } from './modalReducer';
import { postsReducer, PostsState } from './postsReducer';

const rootReducer = combineReducers({
  form: reduxFormReducer,
  modal: modalReducer,
  posts: postsReducer,
});

export type AppState = {
  form?: FormReducer;
  modal: UserState;
  posts: PostsState;
};

export default rootReducer;
