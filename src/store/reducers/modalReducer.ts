import {
  SET_AUTH_MODAL_STATE,
  SET_FORGOT_PASSWORD_STATUS,
  UserAction,
} from '@store/actions/types/modals';

export interface UserState {
  forgotPassword: {
    hasSend: boolean;
  };
  authModal: {
    isOpen: boolean;
  };
}

const initialState: UserState = {
  forgotPassword: {
    hasSend: false,
  },
  authModal: {
    isOpen: false,
  },
};

export const modalReducer = (
  state: UserState = initialState,
  action: UserAction
): UserState => {
  switch (action.type) {
    case SET_FORGOT_PASSWORD_STATUS:
      return {
        ...state,
        forgotPassword: {
          hasSend: action.payload,
        },
      };
    case SET_AUTH_MODAL_STATE:
      return {
        ...state,
        authModal: {
          isOpen: action.payload,
        },
      };
    default:
      return state;
  }
};

export default modalReducer;
