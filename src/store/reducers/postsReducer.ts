import {
  GET_BLOG_POST_FAIL,
  GET_BLOG_POST_REQUEST,
  GET_BLOG_POST_SUCCESS,
  GET_TWITTER_POST_FAIL,
  GET_TWITTER_POST_REQUEST,
  GET_TWITTER_POST_SUCCESS,
  IPost,
  PostsAction,
} from '@store/actions/types/posts';

export interface PostsState {
  twitter: {
    isLoading: boolean;
    post?: IPost;
  };
  blog: {
    isLoading: boolean;
    post?: IPost;
  };
}

const initialState: PostsState = {
  twitter: {
    isLoading: false,
  },
  blog: {
    isLoading: false,
  },
};

export const postsReducer = (
  state: PostsState = initialState,
  action: PostsAction
): PostsState => {
  switch (action.type) {
    case GET_BLOG_POST_REQUEST:
      return {
        ...state,
        blog: {
          ...state.blog,
          isLoading: true,
        },
      };
    case GET_BLOG_POST_SUCCESS:
      return {
        ...state,
        blog: {
          isLoading: false,
          post: action.payload,
        },
      };
    case GET_BLOG_POST_FAIL:
      return {
        ...state,
        blog: {
          ...state.twitter,
          isLoading: false,
        },
      };
    case GET_TWITTER_POST_REQUEST:
      return {
        ...state,
        twitter: {
          ...state.twitter,
          isLoading: true,
        },
      };
    case GET_TWITTER_POST_SUCCESS:
      return {
        ...state,
        twitter: {
          isLoading: false,
          post: action.payload,
        },
      };
    case GET_TWITTER_POST_FAIL:
      return {
        ...state,
        twitter: {
          ...state.twitter,
          isLoading: false,
        },
      };
    default:
      return state;
  }
};

export default postsReducer;
