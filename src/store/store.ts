import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import startMocks from '@mocks/rootMock';
import rootReducer from './reducers/rootReducer';
import rootSaga from './sagas/rootSaga';

startMocks();

const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
  const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
  sagaMiddleware.run(rootSaga);
  return store;
};

export default configureStore;
