import Button from '@components/Button/Button';
import Modal from '@components/Modal/Modal';
import React, { FunctionComponent } from 'react';
import { InjectedFormProps, reduxForm } from 'redux-form';
import LabelledField from '@components/LabelledField/LabelledField';
import { email, required } from '@utils/validators';
import './ModalForgot.scss';
import { ForgotPasswordRequestParams } from '@store/actions/types/forgotPassword';
import { ModalForgotProps } from './ModalForgotProps';

const ModalForgot: FunctionComponent<
  InjectedFormProps<ForgotPasswordRequestParams, ModalForgotProps, string> &
    ModalForgotProps
> = ({
  isOpen,
  closeModalHandler,
  handleSubmit,
  pristine,
  submitting,
  valid,
  closeTimeoutMS,
  hasSend,
}: InjectedFormProps<ForgotPasswordRequestParams, ModalForgotProps, string> &
  ModalForgotProps) => {
  const getModalDescription = (): string =>
    hasSend
      ? 'Thank you, instructions to reset your password have been e-mailed to the address you provided!'
      : 'Please enter the email address associated with your globaledit account to reset your password.';

  const closeModal = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    closeModalHandler(event);
  };

  const modalFooter = (
    <>
      <Button
        kind="small"
        type="submit"
        colorTheme="blue"
        disabled={!!(pristine || submitting || !valid)}
        loading={!!submitting}
        onClick={handleSubmit}
      >
        Submit
      </Button>
      <Button kind="small" colorTheme="gray" onClick={closeModal}>
        Cancel
      </Button>
    </>
  );

  return (
    <Modal
      className="modal-forgot"
      title={hasSend ? 'Email Sent' : 'Password Reset'}
      description={getModalDescription()}
      isOpen={isOpen}
      closeModalHandler={closeModal}
      closeTimeoutMS={closeTimeoutMS}
      footer={!hasSend && modalFooter}
    >
      {!hasSend && (
        <form className="forgot-form" onSubmit={handleSubmit}>
          <LabelledField
            className="modal-forgot__email"
            label="Email Address"
            colorTheme="light"
            name="email"
            validate={[required, email]}
          />
        </form>
      )}
    </Modal>
  );
};

export default reduxForm<ForgotPasswordRequestParams, ModalForgotProps>({
  form: 'forgot-password',
})(ModalForgot);
