import { ForgotPasswordRequestParams } from '@store/actions/types/forgotPassword';
import { MouseEventHandler } from 'react';

export interface ModalForgotProps {
  isOpen: boolean;
  hasSend: boolean;
  closeModalHandler: MouseEventHandler<HTMLButtonElement>;
  closeTimeoutMS?: number;
}

export interface ModalForgotContainerProps extends ModalForgotProps {
  submit: ({ email }: ForgotPasswordRequestParams) => void;
}
