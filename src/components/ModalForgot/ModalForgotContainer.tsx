import React, { Dispatch, FunctionComponent } from 'react';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { forgotPasswordRequest } from '@store/actions/creators/forgotPassword';
import { ForgotPasswordRequestParams } from '@store/actions/types/forgotPassword';
import { AppState } from '@store/reducers/rootReducer';
import ModalForgotComponent from './ModalForgotComponent';
import { ModalForgotContainerProps } from './ModalForgotProps';

const ModalForgotContainer: FunctionComponent<ModalForgotContainerProps> = ({
  submit,
  hasSend,
  ...otherProps
}: ModalForgotContainerProps) => {
  const submitForm = (formValues: ForgotPasswordRequestParams) => {
    submit(formValues);
  };

  return (
    <ModalForgotComponent
      onSubmit={submitForm}
      hasSend={hasSend}
      {...otherProps}
    />
  );
};

const mapStateToProps = (state: AppState) => ({
  hasSend: state.modal.forgotPassword.hasSend,
});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => ({
  submit: ({ email }: ForgotPasswordRequestParams) => {
    dispatch(forgotPasswordRequest(email));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalForgotContainer);
