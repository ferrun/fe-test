import React, { FunctionComponent } from 'react';
import _uniqueId from 'lodash/uniqueId';
import FieldPassword from '@components/FieldPassword/FieldPassword';
import Field from '@components/Field/Field';
import clsx from 'clsx';
import './LabelledField.scss';
import {
  LabelledFieldProps,
  LabelledFieldPropsDefault,
} from './LabelledFieldProps';

const LabelledField: FunctionComponent<LabelledFieldProps> = ({
  className,
  label,
  type,
  id,
  colorTheme,
  ...otherProps
}: LabelledFieldProps) => {
  const fieldId: string = id || _uniqueId('field-');

  return (
    <div className={clsx('labelled-field', colorTheme, className)}>
      <label className="labelled-field__label" htmlFor={fieldId}>
        {label}
      </label>
      {type === 'password' ? (
        <FieldPassword id={fieldId} colorTheme={colorTheme} {...otherProps} />
      ) : (
        <Field
          type={type}
          id={fieldId}
          colorTheme={colorTheme}
          {...otherProps}
        />
      )}
    </div>
  );
};

LabelledField.defaultProps = LabelledFieldPropsDefault;

export default LabelledField;
