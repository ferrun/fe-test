import { FieldProps } from '@components/Field/FieldProps';

export interface LabelledFieldProps extends FieldProps {
  label: string;
  id?: string;
}

export const LabelledFieldPropsDefault: LabelledFieldProps = {
  label: '',
  colorTheme: 'dark',
};
