import { InputHTMLAttributes } from 'react';
import { Validator } from 'redux-form';

export interface FieldProps extends InputHTMLAttributes<HTMLInputElement> {
  className?: string;
  colorTheme?: 'light' | 'dark';
  validate?: Validator | Validator[] | undefined;
}

export const FieldPropsDefault: FieldProps = {
  className: '',
  colorTheme: 'dark',
};
