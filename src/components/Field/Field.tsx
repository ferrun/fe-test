import clsx from 'clsx';
import React, { FunctionComponent } from 'react';
import { Field as ReduxField, WrappedFieldProps } from 'redux-form';
import './Field.scss';
import { FieldProps, FieldPropsDefault } from './FieldProps';

const renderField = ({
  input,
  type,
  className,
  meta: { error, touched },
  ...otherProps
}: FieldProps & WrappedFieldProps) => (
  <div className="field__wrapper">
    <input
      {...input}
      className={clsx(className, { error: error && touched })}
      type={type}
      {...otherProps}
    />
    {touched && error && typeof error === 'string' && (
      <div className="field__error">{error}</div>
    )}
  </div>
);

const Field: FunctionComponent<FieldProps> = ({
  type,
  className,
  colorTheme,
  validate,
  ...otherProps
}: FieldProps) => (
  <ReduxField
    component={renderField}
    className={clsx('field', colorTheme, className)}
    type={type}
    validate={validate}
    {...otherProps}
  />
);

Field.defaultProps = FieldPropsDefault;

export default Field;
