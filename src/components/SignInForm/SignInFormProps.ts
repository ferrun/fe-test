import { SignInRequestParams } from '@store/actions/types/signIn';

export interface SignInFormProps {
  setForgotStatus: (status: boolean) => void;
}

export interface SignInFormContainerProps extends SignInFormProps {
  submit: ({ userName, password }: SignInRequestParams) => void;
  setForgotStatus: (hasSend: boolean) => void;
}
