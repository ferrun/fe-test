import React, { Dispatch, FunctionComponent } from 'react';
import { connect } from 'react-redux';
import { AnyAction } from 'redux';
import { signInRequest } from '@store/actions/creators/signIn';
import { SignInRequestParams } from '@store/actions/types/signIn';
import { setForgotPasswordStatus } from '@store/actions/creators/modal';
import SignInFormComponent from './SignInFormComponent';
import { SignInFormContainerProps } from './SignInFormProps';

const SignInFormContainer: FunctionComponent<SignInFormContainerProps> = ({
  submit,
  setForgotStatus,
}: SignInFormContainerProps) => {
  const submitForm = (formValues: SignInRequestParams) => {
    submit(formValues);
  };

  return (
    <SignInFormComponent
      onSubmit={submitForm}
      setForgotStatus={setForgotStatus}
    />
  );
};

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => ({
  submit: ({ userName, password }: SignInRequestParams) =>
    dispatch(signInRequest(userName, password)),
  setForgotStatus: (hasSend: boolean) =>
    dispatch(setForgotPasswordStatus(hasSend)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignInFormContainer);
