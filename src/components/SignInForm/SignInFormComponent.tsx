import Button from '@components/Button/Button';
import LabelledField from '@components/LabelledField/LabelledField';
import React, { FunctionComponent, useState } from 'react';
import { InjectedFormProps, reduxForm } from 'redux-form';
import ModalForgot from '@components/ModalForgot/ModalForgotContainer';
import {
  alphaNumeric,
  maxLength40,
  minLength3,
  required,
} from '@utils/validators';
import { SignInRequestParams } from '@store/actions/types/signIn';
import { SignInFormProps } from './SignInFormProps';
import './SignInForm.scss';

const SignInForm: FunctionComponent<
  InjectedFormProps<SignInRequestParams, SignInFormProps, string> &
    SignInFormProps
> = ({
  handleSubmit,
  pristine,
  submitting,
  valid,
  setForgotStatus,
}: InjectedFormProps<SignInRequestParams, SignInFormProps, string> &
  SignInFormProps) => {
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const modalCloseTimeout = 300;

  const openModal = () => {
    setModalOpen(true);
  };

  const closeModal = () => {
    setModalOpen(false);
    setTimeout(() => {
      setForgotStatus(false);
    }, modalCloseTimeout);
  };

  return (
    <>
      <form className="sign-in-form" onSubmit={handleSubmit}>
        <LabelledField
          className="sign-in-form__field"
          label="Username"
          name="userName"
          validate={[required, minLength3, maxLength40, alphaNumeric]}
          disabled={!!submitting}
        />
        <LabelledField
          className="sign-in-form__field"
          label="Password"
          type="password"
          name="password"
          validate={[required, minLength3, maxLength40]}
          disabled={!!submitting}
        />
        <div className="sign-in-form__footer">
          <Button
            type="submit"
            colorTheme="blue"
            disabled={!!(pristine || submitting || !valid)}
            loading={!!submitting}
          >
            Sign In
          </Button>
          <Button kind="borderless" onClick={openModal}>
            Forgot password?
          </Button>
        </div>
      </form>
      <ModalForgot
        isOpen={modalOpen}
        closeModalHandler={closeModal}
        closeTimeoutMS={modalCloseTimeout}
      />
    </>
  );
};

export default reduxForm<SignInRequestParams, SignInFormProps>({
  form: 'sign-in',
})(SignInForm);
