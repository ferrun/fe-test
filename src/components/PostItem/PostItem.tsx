import formatDate from '@utils/dates';
import React, { FunctionComponent } from 'react';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import { PostItemProps, PostItemPropsDefault } from './PostItemProps';
import './PostItem.scss';

const PostItem: FunctionComponent<PostItemProps> = ({
  title,
  post,
  maxLength,
  isLoading,
}: PostItemProps) => {
  const getPostText = (text: string) =>
    maxLength && text.length > maxLength
      ? `${text.substr(0, maxLength)}...`
      : text;

  return (
    <SkeletonTheme color="#202020" highlightColor="#444">
      <div className="post-item">
        <p className="post-item__title">{title}</p>
        <p className="post-item__date">
          {post?.date && !isLoading ? (
            formatDate(new Date(post.date))
          ) : (
            <Skeleton />
          )}
        </p>
        <p className="post-item__text">
          {post?.text && !isLoading ? (
            getPostText(post.text)
          ) : (
            <Skeleton count={3} />
          )}
        </p>
      </div>
    </SkeletonTheme>
  );
};

PostItem.defaultProps = PostItemPropsDefault;

export default PostItem;
