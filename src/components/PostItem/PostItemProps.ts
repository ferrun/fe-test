import { IPost } from '@store/actions/types/posts';

export interface PostItemProps {
  title: string;
  isLoading?: boolean;
  post?: IPost;
  maxLength?: number;
}

export const PostItemPropsDefault: PostItemProps = {
  title: '',
  maxLength: 77,
};
