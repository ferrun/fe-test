import { ButtonHTMLAttributes } from 'react';

export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  kind?: 'primary' | 'borderless' | 'small';
  colorTheme?: 'blue' | 'gray';
  loading?: boolean;
  className?: string;
}

export const ButtonsPropsDefault: ButtonProps = {
  type: 'button',
  kind: 'primary',
  colorTheme: 'blue',
  loading: false,
};
