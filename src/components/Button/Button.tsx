import clsx from 'clsx';
import React, { FunctionComponent } from 'react';
import { ReactComponent as Loader } from './assets/preloader.svg';
import { ButtonProps, ButtonsPropsDefault } from './ButtonProps';
import './Button.scss';

const Button: FunctionComponent<ButtonProps> = ({
  type,
  kind,
  colorTheme,
  className,
  children,
  loading,
  ...otherProps
}: ButtonProps) => (
  <button
    className={clsx('button', kind, colorTheme, className, { loading })}
    type={type}
    {...otherProps}
  >
    {loading && <Loader className="button__loader" />}
    {children}
  </button>
);

Button.defaultProps = ButtonsPropsDefault;

export default Button;
