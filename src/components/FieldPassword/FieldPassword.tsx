import Field from '@components/Field/Field';
import { FieldProps } from '@components/Field/FieldProps';
import React, {
  FunctionComponent,
  InputHTMLAttributes,
  useEffect,
  useState,
} from 'react';
import './FieldPassword.scss';

const FieldPassword: FunctionComponent<
  InputHTMLAttributes<HTMLInputElement> & FieldProps
> = ({ ...field }) => {
  const [passwordVisible, setPasswordVisible] = useState<boolean>(false);
  const [currentType, setCurrentType] = useState<string>('password');

  useEffect(() => {
    if (passwordVisible) {
      setCurrentType('text');
    } else {
      setCurrentType('password');
    }
  }, [passwordVisible]);

  const togglePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };

  return (
    <div className="field-password">
      <Field className="field-password__input" type={currentType} {...field} />
      <button
        className="field-password__button"
        type="button"
        onClick={togglePasswordVisibility}
      >
        {passwordVisible ? 'Hide' : 'Show'}
      </button>
    </div>
  );
};

export default FieldPassword;
