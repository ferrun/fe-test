import { MouseEventHandler, ReactChild } from 'react';

export interface ModalProps {
  isOpen: boolean;
  closeModalHandler?: MouseEventHandler<HTMLButtonElement>;
  title: string;
  description?: string;
  className?: string;
  portalClassName?: string;
  children?: ReactChild | false;
  footer?: ReactChild | false;
  closeTimeoutMS?: number;
}

export const ModalPropsDefault: ModalProps = {
  isOpen: false,
  title: '',
  closeTimeoutMS: 300,
};
