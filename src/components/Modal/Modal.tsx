import clsx from 'clsx';
import React, { FunctionComponent } from 'react';
import ReactModal from 'react-modal';
import { ReactComponent as CloseIcon } from './assets/close.svg';
import './Modal.scss';
import { ModalProps, ModalPropsDefault } from './ModalProps';

ReactModal.setAppElement('#root');

const Modal: FunctionComponent<ModalProps> = ({
  isOpen,
  className,
  portalClassName,
  title,
  description,
  closeModalHandler,
  closeTimeoutMS,
  children,
  footer,
}: ModalProps) => (
  <ReactModal
    isOpen={isOpen}
    onRequestClose={closeModalHandler}
    className={clsx('modal', className)}
    overlayClassName="modal__overlay"
    portalClassName={portalClassName}
    shouldCloseOnOverlayClick={false}
    closeTimeoutMS={closeTimeoutMS}
  >
    <div className="modal__heading">
      <div className="modal__title">{title}</div>
      <button className="modal__close" onClick={closeModalHandler}>
        <CloseIcon />
      </button>
    </div>
    <div className="modal__content">
      <div className="modal__description">{description}</div>
      {children}
    </div>
    {footer && <div className="modal__footer">{footer}</div>}
  </ReactModal>
);

Modal.defaultProps = ModalPropsDefault;

export default Modal;
