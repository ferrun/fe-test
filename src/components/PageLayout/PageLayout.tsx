import React, { FunctionComponent } from 'react';
import { PageLayoutProps } from './PageLayoutProps';
import './PageLayout.scss';

const PageLayout: FunctionComponent<PageLayoutProps> = ({
  children,
}: PageLayoutProps) => <div className="page-layout">{children}</div>;

export default PageLayout;
