import { ReactChild } from 'react';

export interface PageLayoutProps {
  children: ReactChild;
}
