import PageLayout from '@components/PageLayout/PageLayout';
import PostItem from '@components/PostItem/PostItem';
import React, { Dispatch, FunctionComponent, useEffect } from 'react';
import { AnyAction } from 'redux';
import SignInForm from '@components/SignInForm/SignInFormContainer';
import { setAuthModalState } from '@store/actions/creators/modal';
import { AppState } from '@store/reducers/rootReducer';
import { useDispatch, useSelector } from 'react-redux';
import Modal from '@components/Modal/Modal';
import {
  getBlogPostRequest,
  getTwitterPostRequest,
} from '@store/actions/creators/posts';
import { ReactComponent as Logo } from './assets/logo-ge-light.svg';
import './SignIn.scss';

const SignIn: FunctionComponent = () => {
  const blogPost = useSelector((state: AppState) => state.posts.blog);

  const twitterPost = useSelector((state: AppState) => state.posts.twitter);

  const isAuthModalOpen = useSelector(
    (state: AppState) => state.modal.authModal.isOpen
  );

  const dispatch: Dispatch<AnyAction> = useDispatch();

  const setAuthModal = (state: boolean) => {
    dispatch(setAuthModalState(state));
  };

  const closeAuthModalHandler = () => {
    setAuthModal(false);
  };

  useEffect(() => {
    dispatch(getTwitterPostRequest());
    dispatch(getBlogPostRequest());
  }, []);

  return (
    <PageLayout>
      <div className="sign-in">
        <div className="sign-in__wrapper">
          <div className="sign-in__heading">
            <h1>Welcome</h1>
            <span className="sign-in__heading-hint">
              Please sign in to continue
            </span>
          </div>
          <Logo className="sign-in__logo" />
          <SignInForm />
          <Modal
            title="Welcome"
            isOpen={isAuthModalOpen}
            closeModalHandler={closeAuthModalHandler}
          >
            Signin successfully completed
          </Modal>
          <div className="sign-in__footer">
            <PostItem
              title="Latest blog post"
              post={blogPost.post}
              isLoading={blogPost.isLoading}
            />
            <PostItem
              title="Recent Tweet"
              post={twitterPost.post}
              isLoading={twitterPost.isLoading}
            />
          </div>
        </div>
      </div>
    </PageLayout>
  );
};

export default SignIn;
