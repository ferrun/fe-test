export interface SignInProps {
  isAuthModalOpen: boolean;
  setAuthModal: (isOpen: boolean) => void;
}
