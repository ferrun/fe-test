import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import '@scss/global.scss';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import SignIn from '@pages/SignIn/SignIn';
import configureStore from '@store/store';

const store = configureStore();

ReactDOM.render(
  <Router>
    <Provider store={store}>
      <Switch>
        <Route exact component={SignIn} />
      </Switch>
    </Provider>
  </Router>,
  document.getElementById('root')
);
