import axios from 'axios';

interface forgotPasswordRequest {
  email: string;
}

interface forgotPasswordResponse {
  success: boolean;
  errors?: object;
}

export async function forgotPassword({
  email,
}: forgotPasswordRequest): Promise<forgotPasswordResponse> {
  const result = await axios.post('/api/forgot', { email });
  return result.data;
}

export default forgotPassword;
