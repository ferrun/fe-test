import { IUser } from '@typings/user';
import axios from 'axios';

interface signInRequest {
  userName: string;
  password: string;
}

interface signInResponse {
  success: boolean;
  user?: IUser;
  errors?: object;
}

export async function signIn({
  userName,
  password,
}: signInRequest): Promise<signInResponse> {
  const result = await axios.post('/api/signin', { userName, password });
  return result.data;
}

export default signIn;
