import { IPost } from '@store/actions/types/posts';
import axios from 'axios';

export async function loadTwitterPost(): Promise<IPost> {
  const result = await axios.get('/api/loadTwitter');
  return result.data;
}

export async function loadBlogPost(): Promise<IPost> {
  const result = await axios.get('/api/loadBlog');
  return result.data;
}
