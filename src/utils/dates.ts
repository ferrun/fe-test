export const formatDate = (date: Date | number) => {
  const options: Intl.DateTimeFormatOptions[] = [
    { month: 'long' },
    { day: 'numeric' },
    { year: 'numeric' },
  ];
  const format = (m: Intl.DateTimeFormatOptions) => {
    const formatted = new Intl.DateTimeFormat('en', m);
    return formatted.format(date);
  };

  const formattedDate = options.map(format);
  return `${formattedDate[0]} ${formattedDate[1]}, ${formattedDate[2]}`;
};

export default formatDate;
