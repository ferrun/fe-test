export const required = (value: string) =>
  value ? undefined : 'This field required';

export const maxLength = (max: number) => (value: string) =>
  value?.length > max ? `Must be ${max} characters or less` : undefined;

export const maxLength40 = maxLength(40);

export const minLength = (min: number) => (value: string) =>
  value?.length < min ? `Must be ${min} characters or more` : undefined;

export const minLength3 = minLength(3);

export const email = (value: string) =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined;

export const alphaNumeric = (value: string) =>
  value && /[^a-zA-Z0-9]/i.test(value)
    ? 'Only alphanumeric characters'
    : undefined;
