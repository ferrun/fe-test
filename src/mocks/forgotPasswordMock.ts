import { AxiosRequestConfig } from 'axios';
import MockAdapter from 'axios-mock-adapter';

export const setForgotPasswordMock = (mock: MockAdapter) => {
  mock.onPost('/api/forgot').reply((ctx: AxiosRequestConfig) => {
    const data = JSON.parse(ctx.data);

    switch (data.email) {
      case 'error@mail.com':
        return [
          200,
          {
            success: false,
            errors: {
              email: 'User with this email not found',
            },
          },
        ];
      default:
        return [
          200,
          {
            success: true,
          },
        ];
    }
  });
};

export default setForgotPasswordMock;
