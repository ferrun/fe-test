import MockAdapter from 'axios-mock-adapter';

export const setPostsMock = (mock: MockAdapter) => {
  mock.onGet('/api/loadTwitter').reply(200, {
    date: 1524665182000,
    text:
      '#HenryStewartEvents are bringing their #CreativeOps show to NYC for the third time',
  });

  mock.onGet('/api/loadBlog').reply(200, {
    date: 1539612382000,
    text: 'Create Efficiency with a Creative Asset Management Platform',
  });
};

export default setPostsMock;
