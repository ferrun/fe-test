import { AxiosRequestConfig } from 'axios';
import MockAdapter from 'axios-mock-adapter';

export const setSignInMock = (mock: MockAdapter) => {
  mock.onPost('/api/signin').reply((ctx: AxiosRequestConfig) => {
    const data = JSON.parse(ctx.data);

    switch (data.userName) {
      case 'John':
        return [
          200,
          {
            success: false,
            errors: {
              userName: 'Username not found',
            },
          },
        ];
      case 'Phill':
        return [
          200,
          {
            success: false,
            errors: {
              userName: true,
              password: "Username and password doesn't match",
            },
          },
        ];
      default:
        return [
          200,
          {
            success: true,
          },
        ];
    }
  });
};

export default setSignInMock;
