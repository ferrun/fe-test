import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import setForgotPasswordMock from './forgotPasswordMock';
import setSignInMock from './signInMock';
import setPostsMock from './postsMock';

export const startMocks = () => {
  const mock = new MockAdapter(axios, { delayResponse: 2000 });

  setSignInMock(mock);
  setForgotPasswordMock(mock);
  setPostsMock(mock);
};

export default startMocks;
